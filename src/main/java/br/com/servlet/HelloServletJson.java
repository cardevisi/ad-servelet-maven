package br.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class HelloServletJson extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, 
			HttpServletResponse response) 
			throws ServletException, IOException {
			
			response.setContentType("application/json");
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.setCharacterEncoding("UTF-8");
			
			//List<Array> = new ArrayList<>();
			//Map<String, Object> mapDt = new HashMap<String, Object>();
			/*	
			 * mapDt.put("gender_sn", 0);
				mapDt.put("age_sn", 0);
				mapDt.put("gender_uol", 0);
				mapDt.put("age_uol", 0);
				mapDt.put("page_views", 2358);
				mapDt.put("unique_visitors", 2000);
			*/
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("101","Sexo Masculino");
			map.put("102","Sexo Feminino");
			map.put("103","Idade - 13 a 17 anos");
			map.put("104","Idade - 18 a 24 anos");
			map.put("105","Idade - 25 a 34 anos");
			map.put("106","Idade - 35 a 44 anos");
			map.put("107","Idade - 45 a 54 anos");
			map.put("108","Idade - 55 a 64 anos");
			map.put("109","Idade - 65 ou mais");
			
			Gson json = new Gson();
			String jsonStr = json.toJson(map);
			
			//Gson json = new Gson();
			//String jsonStr = json.fromJson(json, Slots.class);
			
			PrintWriter out = response.getWriter();
			
			/*{
			"signals":{
						"101":"Sexo Masculino",
						"102":"Sexo Feminino",
						"103":"Idade - 13 a 17 anos",
						"104":"Idade - 18 a 24 anos",
						"105":"Idade - 25 a 34 anos",
						"106":"Idade - 35 a 44 anos",
						"107":"Idade - 45 a 54 anos",
						"108":"Idade - 55 a 64 anos",
						"109":"Idade - 65 ou mais"
			},
			"profiles" : {
				"dt" : {
					"20150101":[
							{
								"gender_sn":"0",
								"age_sn":"0",
								"gender_uol":"0",
								"age_uol":"0",
								"page_views":"23358",
								"unique_visitors":"20000"
							}
					],
					"20150201":[
							{
								"gender_sn":"0",
								"age_sn":"0",
								"gender_uol":"0",
								"age_uol":"0",
								"page_views":"23358",
								"unique_visitors":"20000"
							}
					]
				}
			}
		};*/
			
		//String json = "{"
		//	+	"'config': {"
		//	+		"'movie': 'swf_ad1.swf',"
		//	+	"'flashvars': {"
		//	+		"'clickTag': 'http://www.cardevisi.com',"
		//	+		"'color': '#FFFFFF'"
		//	+	"},"
		//	+		"'quality': 'high',"
		//	+		"'allowScriptAccess': 'always',"
		//	+		"'allowFullScreen':'false',"
		//	+		"'wmode': 'transparent',"
		//	+		"'menu':'false',"
		//	+		"'play':'true'"
		//	+	"},"
		//	+	"'tracking': {"
		//	+		"'start': '1321321564',"
		//	+		"'first_quartle': '1321321564',"
		//	+		"'middle_quartle': '1321321564',"
		//	+		"'complete': '1321321564'"
		//	+	"}"
		//	+"};";
		
		out.println(jsonStr);
	}
}
